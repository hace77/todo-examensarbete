angular.module('Shoppinglista', ['ionic'])


.factory('Projects', function() {
  return {
    all: function() {
      var projectString = window.localStorage['projects'];
      if(projectString) {
        return angular.fromJson(projectString);
      }
      return [];
    },
    save: function(projects) {
      window.localStorage['projects'] = angular.toJson(projects);
    },
    newProject: function(projectTitle) {
      // Add a new project
      return {
        title: projectTitle,
        tasks: []
      };
    },
    getLastActiveIndex: function() {
      return parseInt(window.localStorage['lastActiveProject']) || 0;
    },
    setLastActiveIndex: function(index) {
      window.localStorage['lastActiveProject'] = index;
    },
    delete: function(projectTitle) {
      window.localStorage.clear();
    },  
      
  }
})

.controller('TodoCtrl', function($scope, $timeout, $ionicModal, Projects, $ionicSideMenuDelegate, $ionicGesture, $ionicPopup)  {

  // A utility function for creating a new project
  // with the given projectTitle
  var createProject = function(projectTitle) {
    var newProject = Projects.newProject(projectTitle);
    $scope.projects.push(newProject);
    Projects.save($scope.projects);
    $scope.selectProject(newProject, $scope.projects.length-1);
  }


  // Load or initialize projects
  $scope.projects = Projects.all();

  // Grab the last active, or the first project
  $scope.activeProject = $scope.projects[Projects.getLastActiveIndex()];

  // Called to create a new project
  $scope.newProject = function() {
          navigator.notification.prompt("Skriv in namnet på din lista nedan", addList, "Lägg till lista", ["avbryt","ok"]);

  };
    
function addList(results) {
    
    if (results.buttonIndex == 2 & results.input1 != "") {
          createProject(results.input1);
    } else if (results.buttonIndex == 1)   {
         //cancel
    }
    else {
       $scope.showAlert();
    } 

}
  // Called to select the given project
  $scope.selectProject = function(project, index) {
    $scope.activeProject = project;
    Projects.setLastActiveIndex(index);
    $ionicSideMenuDelegate.toggleRight(false);
  };
    
    // Called to select the given project
  $scope.selectTask = function(task, index) {
    $scope.activeTask = task;
  };
    
  $scope.deleteProject = function(project, index) {
    $scope.activeProject = project;
    Projects.setLastActiveIndex(index);
    $scope.projects.splice(index, 1);
    delete project.tasks;
    delete project;
    localStorage.clear();  
    Projects.save($scope.projects);
  };

  // Create our modal
  $ionicModal.fromTemplateUrl('new-task.html', function(modal) {
    $scope.taskModal = modal;
  }, {
    scope: $scope
  });

  $scope.createTask = function(task) {
    if(!$scope.activeProject || !task) {
      return;
    }
    $scope.activeProject.tasks.push({
      title: task.title
    });
    $scope.taskModal.hide();

    // Inefficient, but save all the projects
    Projects.save($scope.projects);

    task.title = "";
  };

  $scope.newTask = function() {
    $scope.taskModal.show();
  };

  $scope.closeNewTask = function() {
    $scope.taskModal.hide();
  }

  $scope.toggleProjects = function() {
    $ionicSideMenuDelegate.toggleRight();
  };
    
  $scope.deleteTaskConfirm = function(task, $index) {
   var confirmPopup = $ionicPopup.confirm({
     title: 'Ta bort proukt',
     template: 'Är du säker att du vill ta bort denna produkt?'
   });
   confirmPopup.then(function(res) {
     if(res) {
       $scope.deleteTask(task, $index);
     } else {
      //close
     }
   });
   };
    
    $scope.deleteProjectConfirm = function(project, $index) {
   var confirmPopup = $ionicPopup.confirm({
     title: 'Ta bort lista',
     template: 'Är du säker att du vill ta bort denna lista?'
   });
   confirmPopup.then(function(res) {
     if(res) {
       $scope.deleteProject(project, $index)
     } else {
      //close
     }
   });
   };
    
       $scope.showAlert = function() {
   var alertPopup = $ionicPopup.alert({
     title: 'Ange namn!',
     template: 'Du måste döpa din lista'
   });
   alertPopup.then(function(res) {
        $scope.newProject();
   });
 };
    
    
    // Called to delete the given task
  $scope.deleteTask = function(task, index) {
 
    $scope.activeProject.tasks.splice(index, 1);
    localStorage.clear();  
    Projects.save($scope.projects);
      
  };

  // Try to create the first project
  $timeout(function() {
    if($scope.projects.length == 0) {       
        navigator.notification.prompt("Skriv namnet på din lista nedan", addList,"Skapa din första lista", ["avbryt","ok"]);
      

    }
  }, 1000);
    

});


