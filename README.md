# README #

Examensarbete Christian Mårtensson GBJU 13

### VAd är detta###

Detta är ett examensarbete jag gjort för min utbildning på Plushögskolan med Examen 2015

### Avändarinstruktioner ###

När du första gången öppnar appen eller inte har några listor skapade uppmanas du skapa en första lista. Skriv bara in namnet på din lista och tryck ok.
Du ser nu namnet på din lista längst upp i appen. Klicka nu på symbolen längst upp till vänster för att skapa en produkt att lägga i din lista.
Du får nu upp en ruta där du kan skriva in namnet på din produkt, tryck ok och din produkt visas i listan.
Vill du ta bort en produkt ur listan håll bara fingret på den i 0,5 sekunder du får därefter upp en ruta som ber dig bekräfta borttagandet.
Vill du skapa en ny lista så trycker du på symbolen längst upp i högra hörnet, alternativt sveper du med fingret över skärmen åt vänster. Du kommer nu in i menyn där du ser alla dina listor. Skapa en ny lista genom att trycka på + symbolen. Du får nu upp en ruta där du skriver in namnet på din lista, tryck ok för att skapa. du kommer nu direkt komma in i listan och kan lägga till produkter såsom beskrivet ovan.
Vill du ta bort en lista håll bara fingret på den i 0,5 sekunder du får därefter upp en ruta som ber dig bekräfta borttagandet.
